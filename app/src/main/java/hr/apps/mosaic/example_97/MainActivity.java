package hr.apps.mosaic.example_97;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

	@BindView(R.id.vpPager) ViewPager vpPager;
	@BindView(R.id.tlTabs) TabLayout tlTabs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		this.loadFragments();
	}

	private void loadFragments() {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.insertFragment(new NameFragment(),NameFragment.TITLE);
		adapter.insertFragment(new ProfileFragment(),ProfileFragment.TITLE);
		adapter.insertFragment(new DetailsFragment(),DetailsFragment.TITLE);
		this.vpPager.setAdapter(adapter);
		this.tlTabs.setupWithViewPager(this.vpPager);
	}
}
